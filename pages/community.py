# by Sridhar
# for community pages

from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User


@app.route('/community', methods=['GET'])
def community():

	recentUsers = User.find_recent_users(5)
	return render_template("community.html", recentUsers=recentUsers)

