"""Direct Data Edits

Revision ID: 4d9c3b06451
Revises: 00000000
Create Date: 2019-04-19 09:12:23.879828

"""

# revision identifiers, used by Alembic.
revision = '4d9c3b06451'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
	sql = 'UPDATE user SET `point_balance`=5000 WHERE `user_id` = 1'
	result = op.execute(sql)
	sql = 'INSERT INTO rel_user (`user_id`, `rel_lookup`, `attribute`) VALUES (2, "LOCATION", "USA")'
	result = op.execute(sql)
	sql = 'UPDATE user SET `tier`="Silver" WHERE `user_id` = 3'
	result = op.execute(sql)


def downgrade():
	sql = 'UPDATE user SET `point_balance`=0 WHERE `user_id` = 1'
	result = op.execute(sql)
	sql = 'DELETE FROM rel_user WHERE `user_id`=2'
	result = op.execute(sql)
	sql = 'UPDATE user SET `tier`="Carbon" WHERE `user_id` = 3'
	result = op.execute(sql)

