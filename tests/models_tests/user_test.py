from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    # by Sridhar
    # tests for user.is_below_tier
    def test_below_tier(self):
		assert self.chuck.is_below_tier('Platinum') == True
		assert self.chuck.is_below_tier('Gold') == True
		assert self.chuck.is_below_tier('Silver') == True
		assert self.chuck.is_below_tier('Bronze') == True
		assert self.chuck.is_below_tier('Carbon') == True
		assert self.chuck.is_below_tier('G') == None

		assert self.justin.is_below_tier('Platinum') == True
		assert self.justin.is_below_tier('Gold') == True
		assert self.justin.is_below_tier('Silver') == True
		assert self.justin.is_below_tier('Bronze') == False
		assert self.justin.is_below_tier('Carbon') == False
		assert self.justin.is_below_tier('G') == None




